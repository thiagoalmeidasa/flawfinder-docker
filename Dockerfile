FROM registry.gitlab.com/gitlab-org/security-products/analyzers/flawfinder:2

LABEL MAINTAINER Thiago Almeida <thiagoalmeidasa@gmail.com>

RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install --no-install-recommends -y locales \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
#     dpkg-reconfigure --frontend=noninteractive locales && \
#     update-locale LANG=en_US.UTF-8

# ENV LANG en_US.UTF-8

RUN sed -i -e 's/# en_US ISO-8859-1/en_US ISO-8859-1/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.ISO-8859-1

ENV LANG en_US.ISO-8859-1
